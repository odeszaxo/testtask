import List from "./components/List";
import Layout from "./components/UI/Layout";
import "./styles/normalize.scss";

function App() {
  return (
    <Layout>
      <List />
    </Layout>
  );
}

export default App;
