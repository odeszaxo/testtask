import { useSelector, useDispatch } from "react-redux";
import type { RootState } from "../../store/store";
import { useSpring, animated, useTransition } from "@react-spring/web";
import { remove, add } from "../../store/reducers/cardsReducer";
import "./index.scss";

export default function List() {
  const dispatch = useDispatch();
  const cards = useSelector((state: RootState) => state.counter.cards);
  const [springs, api] = useSpring(() => ({
    from: { x: 0 },
  }));
  const transitions = useTransition(cards, {
    from: { opacity: 0, transform: "translateX(-200px)", width: "200px" },
    enter: { opacity: 1, transform: "translateX(0px)" },
    leave: { opacity: 0, transform: "translateX(200px)", width: "0px" },
    trail: 50,
  });

  const handleAdd = () => {
    api.start({
      from: {
        x: -200,
      },
      to: {
        x: 0,
      },
    });
  };

  return (
    <div className="list">
      <div className="list__controls">
        <button
          className="list__control"
          onClick={() => {
            dispatch(
              add(
                "#" +
                  (((1 << 24) * Math.random()) | 0)
                    .toString(16)
                    .padStart(6, "0"),
              ),
            );
            handleAdd();
          }}
        >
          Добавить
        </button>
        <button
          className="list__control"
          onClick={() => {
            dispatch(remove());
          }}
        >
          Удалить
        </button>
      </div>
      <div className="list__cards">
        {transitions((style, item) => (
          <animated.div
            className="list__card"
            key={item}
            style={{
              ...style,
              width: "calc(20% - 5px)",
              background: item,
              borderRadius: 8,
              ...springs,
            }}
          />
        ))}
      </div>
    </div>
  );
}
