import { createSlice } from "@reduxjs/toolkit";

export interface CardsState {
  cards: string[];
}

const initialState: CardsState = {
  cards: [],
};

export const counterSlice = createSlice({
  name: "counter",
  initialState,
  reducers: {
    add: (state, action) => {
      state.cards.unshift(action.payload);
    },
    remove: (state) => {
      state.cards.pop();
    },
  },
});

export const { add, remove } = counterSlice.actions;

export default counterSlice.reducer;
